module Wrappers
  class SessionWrapper

    def self.authenticate_session(shop, &block)
      auth_session(shop)
      block.call
      clear_session
    end

    def self.auth_session(shop)
      shopify_session = ShopifyAPI::Session.new(
      domain: shop.shopify_domain,
      token: shop.shopify_token,
      api_version: shop.api_version
      )

      ShopifyAPI::Base.activate_session(shopify_session)
    end

    def self.clear_session
      ShopifyAPI::Base.clear_session
    end

    private_class_method :auth_session, :clear_session
  end
end
