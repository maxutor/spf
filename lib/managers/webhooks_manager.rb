module Managers
  class WebhooksManager

    def initialize(shop)
      @shop = shop
    end

    def create_webhook(topic, address)
      Wrappers::SessionWrapper.authenticate_session(@shop) do
        ShopifyAPI::Webhook.new(
          {
            topic: topic,
            address: "#{ENV['APP_URL']}/webhooks/#{address}",
            format: "json"
          }
        ).save
      end
    end

    def destroy_webhook(webhook_id)
      Wrappers::SessionWrapper.authenticate_session(@shop) do
        ShopifyAPI::Webhook.find(webhook_id).destroy
      end
      # Wrappers::SessionWrapper.authenticate_session(@shop) do
      #   @webhooks = ShopifyAPI::Webhook.find(:all)
      #   @webhooks.each {|wh| wh.destroy }
      # end
    end
  end
end
