module Managers
  class ProductsManager

    def initialize(webhook)
      @recieved_data = webhook
    end

    def import_variants(variants)
      Variant.import variants, on_duplicate_key_update: {
        conflict_target: [:shopify_variant_id],
        columns: %i[title price weight]
      }
    end

    def fetch_product_data
      {
        spf_id: @recieved_data[:id],
        title: @recieved_data[:title],
        body_html: @recieved_data[:body_html],
        vendor: @recieved_data[:vendor],
        product_type: @recieved_data[:product_type]
      }
    end

    def fetch_variants_data(product_id)
      @recieved_data[:variants].map do |variant_data|
        {
          shopify_variant_id: variant_data[:id],
          title: variant_data[:title],
          price: variant_data[:price],
          weight: variant_data[:weight],
          product_id: product_id
        }
      end
    end
  end
end
