class ProductsDeleteJob < ActiveJob::Base
  def perform(shop_domain:, webhook:)
    shop = Shop.find_by(shopify_domain: shop_domain)

    if shop.nil?
      logger.error("#{self.class} failed: cannot find shop with domain '#{shop_domain}'")
      return
    end

    shop.with_shopify_session do
      Product.find_by!(spf_id: webhook[:id]).destroy
    rescue ActiveRecord::RecordNotFound
      p 'this product wasnt been saved'
    end
  end
end
