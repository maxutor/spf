class ProductsUpsertJob < ActiveJob::Base

  def perform(shop_domain:, webhook:)
    shop = Shop.find_by(shopify_domain: shop_domain)

    if shop.nil?
      logger.error("#{self.class} failed: cannot find shop with domain '#{shop_domain}'")
      return
    end

    shop.with_shopify_session do
      products_m = Managers::ProductsManager.new(webhook)

      product_data = products_m.fetch_product_data.merge!(shop_id: shop.id)

      product = Product.find_or_create_by(shop_id: shop.id, spf_id: webhook[:id])
      product.update(product_data)

      variants = products_m.fetch_variants_data(product.id)
      products_m.import_variants(variants)
    end
  end
end
