class DownloadProductsJob
  include Sidekiq::Worker

  def perform(shop_id)
    @products = []
    @variants = []

    shop = Shop.find(shop_id)

    Wrappers::SessionWrapper.authenticate_session(shop) do
      @products_data = ShopifyAPI::Product.find(:all)

      shop.with_shopify_session do
        save_data(shop.id)
      end
    end
  end

  private

  def save_data(shop_id)
    prepare_products_data(shop_id)

    import_products
  end

  def prepare_products_data(shop_id)
    @products_data.each do |product_data|
      product = Product.new(
        spf_id: product_data.id,
        title: product_data.title,
        body_html: product_data.body_html,
        vendor: product_data.vendor,
        product_type: product_data.product_type,
        shop_id: shop_id
      )
      product = prepare_variants_data(product_data, product)
      @products << product
    end
  end

  def prepare_variants_data(product_data, product)
    product_data.variants.each do |variant|
      product.variants.new(
        shopify_variant_id: variant.id,
        title: variant.title,
        price: variant.price,
        weight: variant.weight,
        product_id: product.id
      )
    end
    product
  end

  def import_products
    Product.import @products, batch_size: 5, recursive: true
  end
end
