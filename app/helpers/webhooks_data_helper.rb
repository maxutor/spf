module WebhooksDataHelper
  WEBHOOKS_DATA = [
    {
      topic: 'products/create',
      address: 'products_create'
    },
    {
      topic: 'products/update',
      address: 'products_update'
    },
    {
      topic: 'products/delete',
      address: 'products_delete'
    }
  ]
end
