class CustomWebhooksController < ApplicationController
  include ShopifyApp::WebhookVerification

  def products_upsert
    params.permit!
    ProductsUpsertJob.perform_later(shop_domain: shop_domain, webhook: webhook_params.to_h)
    head :no_content
  end

  def products_delete
    params.permit!
    ProductsDeleteJob.perform_later(shop_domain: shop_domain, webhook: webhook_params.to_h)
    head :no_content
  end

  private

  def webhook_params
    params.except(:controller, :action, :type)
  end
end
