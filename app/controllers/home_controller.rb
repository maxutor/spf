# frozen_string_literal: true
class HomeController < AuthenticatedController
  before_action :set_webhook_manager, only: [:create_webhook, :destroy_webhook]

  def index
    @products = ShopifyAPI::Product.find(:all, params: { limit: 10 })
    @webhooks = ShopifyAPI::Webhook.find(:all)
  end

  def create_webhook
    @webhook_manager.create_webhook(params[:topic], params[:address])
    redirect_to '/'
  end

  def destroy_webhook
    @webhook_manager.destroy_webhook(params[:webhook_id])
    redirect_to '/'
  end

  private

  def set_webhook_manager
    @webhook_manager = Managers::WebhooksManager.new(current_shop)
  end

  def current_shop
    Shop.find_by(shopify_domain:  ShopifyAPI::Shop.current.domain)
  end
end
