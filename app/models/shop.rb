# frozen_string_literal: true
# require './app/jobs/download_products_job'
class Shop < ActiveRecord::Base
  include ShopifyApp::ShopSessionStorage

  has_many :products, dependent: :destroy

  after_create :download_products

  def api_version
    ShopifyApp.configuration.api_version
  end

  private

  def download_products
    DownloadProductsJob.perform_async(id)
  end
end
