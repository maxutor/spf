Rails.application.routes.draw do
  root :to => 'home#index'

  post '/home/:webhook_id', to: 'home#destroy_webhook'
  post '/home/:topic/:address', to: 'home#create_webhook'

  post '/webhooks/products_create', to: 'custom_webhooks#products_upsert'
  post '/webhooks/products_update', to: 'custom_webhooks#products_upsert'
  post '/webhooks/products_delete', to: 'custom_webhooks#products_delete'

  mount ShopifyApp::Engine, at: '/'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
