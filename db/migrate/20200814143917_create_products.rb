class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.belongs_to :shop

      t.string :title
      t.integer :spf_id, limit: 8
      t.string :body_html
      t.string :vendor
      t.string :product_type
    end
  end
end
