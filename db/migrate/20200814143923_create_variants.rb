class CreateVariants < ActiveRecord::Migration[5.2]
  def change
    create_table :variants do |t|
      t.belongs_to :product

      t.integer :shopify_variant_id, limit: 8
      t.string :title
      t.integer :price
      t.float :weight

      t.index :shopify_variant_id, unique: true
    end
  end
end
